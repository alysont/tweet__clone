class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  protected
  
  def current_user
    @user = logged_in? ? User.find(session[:user]) : nil
  end
  
  def get_user_by_id(user_id = nil)
    !!user_id ? User.find(user_id) : User.find(params[:id])
  end
  
  def logged_in?
    !!session[:user]
  end
  
  def redirect_if_logged_in!
    redirect_to login_path unless logged_in?
  end
  
end
