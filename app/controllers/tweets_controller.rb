class TweetsController < ApplicationController
  before_action :redirect_if_logged_in!
  before_action :current_user, only: [:new, :create, :destroy]   
  
  def index
    @tweets = Tweet.where(user_id: params[:user_id])  
  end
  
  def new
    @tweet = @user.tweets.new
  end
  
  def create
    @tweet = @user.tweets.create(tweet_params)
    if @tweet.save
      flash[:success] = 'you''ve posted new tweet'
      redirect_to '/index/index'
    else
      flash[:danger] = 'failed to create tweet'
      render 'new'
    end
  end
  
  def destroy
    tweet = Tweet.where(id: params[:id]).first
    if(!!tweet && tweet.user == @user)
      tweet.destroy
      flash[:success] = 'deleted tweet'
    else
      flash[:danger] = 'failed deletion tweet'
    end
    
    redirect_to '/index/index'
  end
  

  
  private
  
  def tweet_params
    params.require(:tweet).permit([:post])
  end
end
