class UsersController < ApplicationController
  before_action :redirect_if_logged_in!, only: [:show, :destroy, :edit, :update]
  before_action :get_user, only: [:show, :destroy, :edit, :update]
  
  def index
    redirect_to login_path
  end
  
  def show
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_param) 
    if @user.save
      flash[:success] = 'user created successfully'
      redirect_to users_path
    else
      flash[:danger] = 'invalid input'
      render 'new'
    end
  end
  
  def destroy
    @user.destroy
    redirect_to users_path
  end
  
  def edit
  end
  
  def update
    if @user.update_attributes(user_param)
      redirect_to users_path
    else
      flash[:danger] = '更新に失敗しました'
      render 'edit'
    end
  end
  
  
  private
  
  def user_param
    params.require(:user).permit([:name, :email, :password, :password_confirmation])
  end
  
  def get_user
    @user = get_user_by_id(params[:id])
  end

end
