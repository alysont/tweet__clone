class FollowsController < ApplicationController
  before_action :redirect_if_logged_in!
  before_action :current_user
  
  def follow
    target_user = get_user_by_id(params[:id])
    if !!target_user
      @user.followers << target_user
      flash[:success] = "followed user"
      redirect_to target_user
    else
      flash[:danger] = "failed following user"
      redirect_to '/index/index'
    end
  end
  
  def unfollow
    target_user = get_user_by_id(params[:id])
    if !!target_user
      @user.followers.delete target_user
      flash[:success] = "unfollowed user"
      redirect_to target_user
    else
      flash[:danger] = "failed unfollowing user"
      redirect_to '/index/index'
    end
  end

end
