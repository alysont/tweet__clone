class SessionsController < ApplicationController
  
  # login画面まで
  def new
    @user = User.new 
  end
  
  # login logic
  def create
    user = User.where(email: params[:user][:email]).first
    # password check
    
    user = user.authenticate(params[:user][:password]) if !!user
    # !!は nil, false等であれは否定の否定を行うことで存在:true 非存在:false
    if !!user
      session[:user] = user.id
      redirect_to '/index/index'
    else
      flash[:danger] = '認証に失敗しました'
      render action: :new
    end
  end
  
  # logout logic
  def destroy
    reset_session
    render action: :new
  end
end
