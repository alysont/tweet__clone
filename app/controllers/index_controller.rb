class IndexController < ApplicationController

  def index
    @user = current_user
    @tweets = Tweet.order("created_at desc")
  end
end
