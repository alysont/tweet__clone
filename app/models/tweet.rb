class Tweet < ActiveRecord::Base
  belongs_to :user
  
  validates :post, presence: true, length: { minimum: 5, maximum: 140 }
end
