class User < ActiveRecord::Base
  has_many :tweets, dependent: :destroy

  has_many :followee_relationships, class_name: "UserRelation",
                                       foreign_key: "followee_id",
                                       dependent: :destroy
  has_many :followees, through: :followee_relationships, source: :follower
  
  has_many :follower_relationships, class_name: "UserRelation",
                                       foreign_key: "follower_id",
                                       dependent: :destroy
  has_many :followers, through: :follower_relationships, source: :followee
  
  


  has_secure_password
  validates :password, length: { minimum: 6 }

  
  validates :name, presence: true,
    length: { minimim: 3, maximum: 20}

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i 
  validates :email, presence: true,
    format: { with: VALID_EMAIL_REGEX },     
    uniqueness: { case_sensitive: false }
    
  def following?(user_id)
    followees.pluck(:id).include? user_id
  end

  def following?(user_id)
    followees.pluck(:id).include? user_id
  end

end
