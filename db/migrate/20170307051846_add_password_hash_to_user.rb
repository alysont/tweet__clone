class AddPasswordHashToUser < ActiveRecord::Migration
  def change
    # ここでは簡易で制約は入れません
    add_column :users, :password_digest, :string
  end
end
