class CreateUserRelations < ActiveRecord::Migration
  def change
    create_table :user_relations do |t|
      t.references :follower, index: true
      t.references :followee, index: true

      t.timestamps null: false
    end
  end
end
